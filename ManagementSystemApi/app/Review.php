<?php

namespace App;

use App\Product;
use Illuminate\Database\Eloquent\Model;

//relationship between review and product
class Review extends Model
{
    protected $timestamp=false;

    public function products()
    {
        return $this->belongsTo(Product::class);
    }
}
