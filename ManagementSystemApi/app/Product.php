<?php

namespace App;

use App\Review;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

//relationship between the product and review
class Product extends Model
{
    use Notifiable;


    protected $timestamp =true;
    protected $fillable = [
        'name', 'details', 'price','stock'
    ];

    public function reviews()
    {
        return $this->hasMany(Review::class);
    }
}
