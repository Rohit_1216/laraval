<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProducTrequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

     //output format for product
    public function rules()
    {
        return [
            'name' => 'required|max:255|unique:products',
            'details'=> 'required|max:255',
            'price'=>'required|max:20',
            'stock'=>'required|max:10',

        ];
    }
}
