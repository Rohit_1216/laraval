<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Requests\ProducTrequest;
use App\Http\Resources\Product\ProductResource;
use App\Http\Resources\Product\ProductCollection;
use Illuminate\Auth\Middleware\Authenticate;

// use Symfony\Component\HttpFoundation\Request;
class ProductController extends Controller
{

    public function __construct()
    {
    //   $this->middleware('auth:api', ['except' => ['index','show']]);
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return  ProductCollection::collection(Product::paginate(10));
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProducTrequest $request)
    {

       $product = Product::create([
            'name' => $request->get('name'),
            'details' => $request->get('details'),
            'price' => $request->get('price'),
            'stock' => $request->get('stock')
        ]);
        return $product;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return new ProductResource($product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        // dd($request->get('name'));
        // dd($request);
       $product = Product::where('id',$id)->update([
                'name' => $request->get('name'),
                'details' => $request->get('details'),
                'price' => $request->get('price'),
                'stock' => $request->get('stock')
            ]);

        // $product = Product::update([
        //     'name' => $request->get('name'),
        //     'details' => $request->get('details'),
        //     'price' => $request->get('price'),
        //     'stock' => $request->get('stock')
        // ]);
        // dd($product);
        return $product;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::where('id',$id)->delete();
        return ($product);
    }
}
