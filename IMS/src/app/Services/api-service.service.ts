import { TokenService } from './token.service';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SignupComponent } from '../components/signup/signup.component';
@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {

  constructor(private http: HttpClient) { }
  private baseUrl = 'http://127.0.0.1:8000/api';

  private httpOptions={
    headers: new HttpHeaders({
      'Content-Type':'application/x-www-form-urlencoded',
      'Authorization':'Bearer '+localStorage.getItem('token')
    })

  };

  signUp(signupData) {
    return this.http.post(`${this.baseUrl}/signup`, signupData);
  };

  login(loginData) {
    return this.http.post(`${this.baseUrl}/login`, loginData);
  };

  products()
  {
    return this.http.get(`${this.baseUrl}/products`,this.httpOptions);
  }

  ProductEdit()
  {

  }

  ProductDelete()
  {

  }
}
