import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TokenService {
  private logic =
  {
    login: 'http://127.0.0.1:8000/api/login',
    signup: 'http://127.0.0.1:8000/api/signup'
  };


  constructor() { }


  handle(token)
  {
    this.set(token);
  }

  set(token) {
    localStorage.setItem('token', token);
    console.log(this.payload(token));
  }

  get() {
    return localStorage.getItem('token');
  }


  remove() {
    return localStorage.clear();
  }

  //
  isValid() {
    const token = this.get();

    if (this.get())
    {
      const payload = this.payload(token);
      if (payload) {
        return Object.values(this.logic).indexOf("iss") ? true : false;
      }
    }
    return false;
  }

  payload(token)
  {
    const payload = token.split(".");
    return this.decode(payload);
  }

  decode(payload)
  {
    return btoa(payload);
  }

  loggedIn()
  {
    return this.isValid();
  }
}
