import { Router } from '@angular/router';
import { TokenService } from './../../Services/token.service';
import { ApiServiceService } from './../../Services/api-service.service';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../Services/auth.service';
// import { ApiServiceService} from 's'
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private api: ApiServiceService,
    private token: TokenService,
    private route: Router,
    private Auth: AuthService
    ) { }

//getting input fields value from Form
//object of form
  public form =
  {
    email: null,
    password: null,
  };

  public error = null;

  //this method call the api
  onSubmit() {
    this.api.login(this.form).subscribe(
      data => this.response(data),
      error => this.handleError(error)
    ); }

    //to get error and show it on the form
  handleError(error) {
    this.error = error.error;
  }
  //this method will get data from Api and save the token
  response(data) {
    this.token.handle(data.access_token);
    this.Auth.changeAuthStatus(false);
    this.route.navigateByUrl('/Profile');
  }

  ngOnInit() {}


}
