import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from '../../Services/api-service.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  error: any;
  data: any;
  ArrayData: any = [];
  PageLinks: any = {};

  constructor(private api: ApiServiceService) { }

  ngOnInit() {
    this.api.products()
    .subscribe(data => {(console.log(data));this.response(data)});
    error => this.handleError(error);
  }

  response(data) {
    this.ArrayData = data.data;
    this.PageLinks=data.links;

  }

  handleError(error) {
    this.error = error.error.errors;
  }




}
