import { ApiServiceService } from './../../Services/api-service.service';
import { Component, OnInit } from '@angular/core';
// import { HttpClientModule, HttpClient } from '@angular/common/http';

// import { ApiServiceService } from '../../services/jarwis.service';
import { TokenService } from './../../Services/token.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  public form = {
    name: null,
    email: null,
    password: null,
    password_confirmation: null
  };
  public error = [];

  constructor(
    private api: ApiServiceService,
    private token: TokenService,
    private route: Router
  ) { }

  onSubmit() {
    this.api.signUp(this.form).subscribe(
      data => this.response(data),
      error => this.handleError(error)
    );
  }

  response(data) {
    this.token.handle(data.access_token);
    this.route.navigateByUrl('/Profile');
  }

  handleError(error) {
    this.error = error.error.errors;
  }

  ngOnInit() {
  }


}
