import { Component, OnInit } from '@angular/core';
import { TokenService } from './../../Services/token.service';
import { Router } from '@angular/router';
import { ApiServiceService } from './../../Services/api-service.service';

@Component({
  selector: 'app-product-update',
  templateUrl: './product-update.component.html',
  styleUrls: ['./product-update.component.css']
})
export class ProductUpdateComponent implements OnInit {


  public form = {
    ProductName: null,
    ProductPrice: null,
    ProductStock: null,
  };s
  constructor(
    private api: ApiserviceService,
    private token: TokenService,
    private route: Router
  ) { }

  onSubmit() {
    this.api.ProductUpdate(this.form).subscribe(
      data => this.response(data),
      error => this.handleError(error)
    );
  }

  response(data) {
    this.token.handle(data.access_token);
    this.route.navigateByUrl('/Profile');
  }




  ngOnInit() {
  }

}
